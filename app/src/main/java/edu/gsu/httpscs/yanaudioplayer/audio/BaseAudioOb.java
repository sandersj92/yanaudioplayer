package edu.gsu.httpscs.yanaudioplayer.audio;

/**
 * Created by Yan on 3/15/17.
 */

public class BaseAudioOb {
    private String URL;
    private String Name;

    public String getURL() {
        return URL;
    }

    public String getName() {
        return Name;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public void setName(String Name) { this.Name = Name;}
}
